from murr_game import mpv


def check_min_skill_value(value: int) -> bool:
    """
    Проверка на минимальное значение атрибута.
    """
    return value >= mpv['MIN_SKILL_VALUE']
