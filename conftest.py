import pytest
from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient

from murr_card.models import Category, MurrCard, MurrCardStatus
from murr_comments.models import Comment
from murren.models import TrustedRegistrationEmail

Murren = get_user_model()


@pytest.fixture(autouse=True)
def override_settings():
    settings.CHANNEL_LAYERS = {
        "default": {
            "BACKEND": "channels.layers.InMemoryChannelLayer"
        }
    }


@pytest.fixture(scope='function')
def count():
    return 5


@pytest.fixture(autouse=True)
def create_trusted_email_service(murren_data):
    TrustedRegistrationEmail.objects.create(service_domain=murren_data['email'].split('@')[1])


@pytest.fixture(scope='function')
def murren_data():
    return {
        'username': 'TestMurrenName',
        'password': 'SecretPassword',
        'email': 'test_email@mail.ru',
        'is_banned': False,
    }


@pytest.fixture(scope='function')
def create_murren(murren_data):
    return Murren.objects.create_user(**murren_data)


@pytest.fixture(scope='function')
def create_murren_list(murren_data, count):
    murren_list = []
    for i in range(count):
        new_murren_data = murren_data.copy()
        new_murren_data.update({
            'email': f'{i}_{murren_data["email"]}',
            'username': f'{murren_data["username"]}_{i}',
        })
        murren = Murren.objects.create_user(**new_murren_data)
        murren_list.append(murren)
    return murren_list


@pytest.fixture
def create_category():
    return Category.objects.create(name='Other', slug='other')


@pytest.fixture(scope='function')
def murr_card_data():
    return {
        'title': 'test murr card',
        'content': 'test murr card content',
        'tags': ['sex', 'rock', 'pony'],
    }


@pytest.fixture(scope='function')
def murr_card_release_data(murr_card_data):
    return {
        **murr_card_data,
        'status': MurrCardStatus.RELEASE
    }


@pytest.fixture(scope='function')
def murr_card_draft_data(murr_card_data):
    return {
        **murr_card_data,
        'status': MurrCardStatus.DRAFT
    }


@pytest.fixture(scope='function')
def murr_card_moderation_data(murr_card_data):
    return {
        **murr_card_data,
        'status': MurrCardStatus.MODERATION
    }


@pytest.fixture(scope='function')
def create_release_murr(create_murren, create_category, murr_card_release_data):
    murr = MurrCard.objects.create(
        owner=create_murren,
        category=create_category,
        **murr_card_release_data
    )
    return {
        'murren': create_murren,
        'murr_card': murr,
    }


@pytest.fixture(scope='function')
def create_release_murr_list(create_murren, create_category, murr_card_release_data, count):
    murr_card_list = []
    for i in range(count):
        murr_card_data = murr_card_release_data.copy()
        murr_card_data.update({
            'title': f'{murr_card_release_data["title"]} {i}',
            'content': f'{murr_card_release_data["content"]} {i}',
        })
        murr = MurrCard.objects.create(
            owner=create_murren,
            category=create_category,
            **murr_card_data)
        murr_card_list.append(murr)
    return {
        'murren': create_murren,
        'murr_card_list': murr_card_list,
    }


@pytest.fixture(scope='function')
def create_draft_murr(create_murren, create_category, murr_card_draft_data):
    murr = MurrCard.objects.create(
        owner=create_murren,
        category=create_category,
        **murr_card_draft_data,
    )
    return {
        'murren': create_murren,
        'murr_card': murr,
    }


@pytest.fixture(scope='function')
def create_moderation_murr(create_murren, murr_card_moderation_data):
    murr = MurrCard.objects.create(
        owner=create_murren,
        category=create_category,
        **murr_card_moderation_data,
    )
    return {
        'murren': create_murren,
        'murr_card': murr,
    }


@pytest.fixture(scope='function')
def create_murren_is_banned(murren_data):
    is_banned_murren_data = murren_data.copy()
    is_banned_murren_data.update({
        'is_banned': True,
        'email': f'is_banned_{murren_data["email"]}',
        'username': f'is_banned_{murren_data["username"]}',
    })
    return Murren.objects.create_user(**is_banned_murren_data)


@pytest.fixture(scope='function')
def murr_comment_data():
    return {
        'text': 'test comment',
    }


@pytest.fixture(scope='function')
def create_murr_comment(create_release_murr, murr_comment_data):
    murren = create_release_murr['murren']
    murr = create_release_murr['murr_card']
    murr_comment = Comment.objects.create(
        author=murren,
        murr=murr,
        **murr_comment_data,
    )
    return {
        'murren': murren,
        'murr_card': murr,
        'murr_comment': murr_comment,
    }


@pytest.fixture(scope='function')
def api_client():
    return APIClient()


@pytest.fixture(scope='function')
def create_child_murr_comment(create_murr_comment, murr_comment_data):
    murren = create_murr_comment['murren']
    murr = create_murr_comment['murr_card']
    murr_comment = create_murr_comment['murr_comment']

    child_murr_commend_data = murr_comment_data.copy()
    child_murr_commend_data.update({
        'text': f'{murr_comment_data["text"]}_children',
    })

    child_murr_comment = Comment.objects.create(
        author=murren,
        murr=murr,
        parent=murr_comment,
        **murr_comment_data,
    )
    return {
        'murren': murren,
        'murr_card': murr,
        'murr_comment': murr_comment,
        'child_murr_comment': child_murr_comment,
    }
