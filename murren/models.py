import time

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _


def murren_upload_to_path_avatar(instance, tmp_image_name):
    filename = str(instance.id) + f"__{time.time()}.jpeg"
    date = time.strftime("%Y/%m/%d", time.localtime())
    return f'profile/avatar/{date}/{filename}'


class Murren(AbstractUser):
    email = models.EmailField(unique=True)
    murren_avatar = models.ImageField(default='default_murren_avatar.png', upload_to=murren_upload_to_path_avatar,
                                      verbose_name='Аватар Муррена')
    is_banned = models.BooleanField(_('banned'), default=False,
                                    help_text=_('Designates whether this user should be treated as banned.'))
    description = models.CharField(max_length=255, default='', blank=True)
    subscribers = models.ManyToManyField('self', related_name='subscriptions', symmetrical=False, blank=True)

    show_murr_card_notifications = models.BooleanField(_('show murr_card notifications?'), default=True)
    show_subscription_notifications = models.BooleanField(_('show subscription notifications?'), default=True)

    def __str__(self):
        return self.username

    def in_subscribers(self, user):
        return user.id in self.subscribers.values_list('id', flat=True)


class TrustedRegistrationEmail(models.Model):
    service_domain = models.CharField(_("Service's domain"), max_length=100)

    class Meta:
        verbose_name = 'Доверенный почтовый сервис'
        verbose_name_plural = 'Доверенные почтовые сервисы'

    def __str__(self):
        return self.service_domain
