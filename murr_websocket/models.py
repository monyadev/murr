from enum import Enum

from django.contrib.auth import get_user_model
from django.db import models
from django_enum_choices.fields import EnumChoiceField

Murren = get_user_model()


class MurrWebSocketType(Enum):
    MURR_CHAT = 'murr_chat'
    MURR_BATTLE_ROOM = 'murr_battle_room'
    SERVICE = 'service'


class MurrWebSocket(models.Model):
    murr_ws_name = models.CharField(max_length=244, unique=True)
    murr_ws_type = EnumChoiceField(MurrWebSocketType)

    def __str__(self):
        return f'{self.murr_ws_type.value}__{self.murr_ws_name}'

    @property
    def link(self):
        return f'/ws/{self.murr_ws_type.value}/{self.murr_ws_name}/'


class MurrWebSocketMembers(models.Model):
    murr_ws_member = models.ForeignKey(Murren, related_name='murr_ws_member', on_delete=models.CASCADE, null=True)
    murr_ws_instance = models.ForeignKey(MurrWebSocket, related_name='murr_ws_member', on_delete=models.CASCADE,
                                         null=True)
    is_online = models.BooleanField(default=False, blank=False, null=False)

    def set_online(self, status: bool):
        self.is_online = status
        self.save(update_fields=["is_online"])

    def __str__(self):
        return self.murr_ws_member.username


class MurrWebSocketMessage(models.Model):
    murr_ws_member = models.ForeignKey(Murren, related_name='murren_ws_message', on_delete=models.CASCADE, null=True)
    murr_ws_instance = models.ForeignKey(MurrWebSocket, related_name='murr_ws_message', on_delete=models.CASCADE,
                                         null=True)
    murr_ws_message = models.TextField(default='')

    def __str__(self):
        return self.murr_ws_message
