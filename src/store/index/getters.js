export const getters = {
  showRegisterForm_getters(state) {
    return state.showRegisterForm;
  },
  showLoginForm_getters(state) {
    return state.showLoginForm;
  },
  showResetPasswordForm_getters(state) {
    return state.showResetPasswordForm;
  },
};
