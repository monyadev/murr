import Vue from "vue";
import Router from "vue-router";

import store from "@/store/index";

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("./views/Home.vue"),
    },
    {
      path: "/login",
      name: "login",
      meta: {},
      component: () => import("./components/murren/Login.vue"),
    },
    {
      path: "/signup",
      name: "signup",
      meta: {},
      component: () => import("./components/murren/SignUp.vue"),
    },
    {
      path: "/murren/my_likes",
      name: "my_likes",
      meta: { accessTokenExpected: true },
      component: () =>
        import("./components/murren/profile/MurrCardMyLikesList.vue"),
    },
    {
      path: "/murren/:murren_name",
      name: "murren",
      meta: { accessTokenExpected: true },
      component: () => import("./components/murren/profile/MurrenProfile.vue"),
    },
    {
      path: "/murren_email_activate",
      name: "murren_email_activate",
      meta: {},
      component: () => import("./components/murren/ConfirmEmail.vue"),
    },
    {
      path: "/set_new_password",
      name: "set_new_password",
      meta: {},
      component: () => import("./components/murren/SetNewPassword.vue"),
    },
    {
      path: "/oauth/vk",
      name: "vk_oauth",
      meta: {},
      component: () => import("./components/murren/oauth/VkOauthCallback.vue"),
    },
    {
      path: "/murr_card/:id",
      name: "murr_card",
      meta: {},
      component: () => import("./components/murr_card/MurrCardDetail.vue"),
    },
    {
      path: "/about",
      name: "about",
      meta: {},
      component: () => import("./views/common/About.vue"),
    },
    {
      path: "*",
      name: "page_404",
      meta: {},
      component: () => import("./views/common/Page404.vue"),
    },
  ],
});

router.beforeEach((to, from, next) => {
  const isTokenExpectedAndExist =
    (to.matched.some((r) => r.meta.accessTokenExpected) &&
      store.state.auth.currentMurren) ||
    !to.matched.some((r) => r.meta.accessTokenExpected);
  if (isTokenExpectedAndExist) {
    return next();
  }
  store.dispatch("popUpMessage", {
    message: "Требуется авторизация 😳",
    type: "warning",
  });
  router.push("/");
  store.dispatch("changeShowLoginForm_actions");
});
export default router;

// workaround for push to "/some_url" when client on "/some_url"
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};
//

Vue.use(Router);
